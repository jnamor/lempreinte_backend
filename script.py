import os
import sys

import olca
import pandas as pd
import arrow as ar
import time

def mean(lst):
    return sum(lst) / len(lst)

def main(parameters):
    """ Prendre en entrée les paramètres de l'interface de saisie
        Envoyer ceux-ci à OpenLCA
        Récupérer en sortie les 16 impacts environnementaux
    """

    start = ar.now()
    # make sure that you started an IPC server with the specific database in
    # openLCA (Window > Developer Tools > IPC Server)
    client = olca.Client(8080)

    # first we read the parameter sets; they are stored in a data frame where
    # each column is a different parameter set
	# 1st column: parameter UUID
	# 2nd column: parameter name
	# last column: process name
    parameters = parameters.astype(float)

    # we prepare a calculation setup for the given LCIA method and reuse it
    # for the different product systems in the database
    calculation_setup = prepare_setup(client, 'Environmental Footprint (Mid-point indicator)')

    # we run a calculation for each combination of parameter set and product
    # system that is in the database
    listImpacts = []
    listGlobalPefScore = []
    listGlobalTime = []
    for system in client.get_descriptors(olca.ProductSystem):
        print('Run calculations for product system %s (%s)' %
              (system.name, system.id))
        calculation_setup.product_system = system
        for index, parameter_set in enumerate(parameters):
            set_parameters(calculation_setup, parameters, parameter_set)
            
            try:
                calc_start = time.time()
                result = client.calculate(calculation_setup)
                calc_finish = time.time() - calc_start
                print('  . calculation finished in', calc_finish)

                # Enregistrer le résultat sous forme .xlsx
                excel_file = '%s_%d.xlsx' % (system.id, parameter_set)
                export_and_dispose(client, result, excel_file)

                # Récupèrer ce fichier excel pour afficher
                # uniquement les impacts environnementaux
                dictImpacts = pd.read_excel(excel_file, sheet_name=2, header=1, index_col="Impact category").iloc[:, 1:][['Result']].to_dict()
                
                # Utiliser les impacts pour calculer le score PEF
                coef_e = (dictImpacts['Result']['Eutrophication, freshwater'] / 2.55) * 0.0685
                coef_gw = (dictImpacts['Result']['Climate change'] / 7760) * 0.515
                coef_rd = (dictImpacts['Result']['Resource use, fossils'] / 65300) * 0.207
                coef_ws = (dictImpacts['Result']['Water use'] / 11500) * 0.2096
                pef_score = (coef_e + coef_gw + coef_rd + coef_ws) * 1000000

                dictImpacts['Product Reference'] = f'F10193{index + 1}E'
                dictImpacts['PEF Score'] = pef_score
                dictImpacts['Time'] = calc_finish

                # Supprimer le fichier en local
                os.remove(excel_file)

                listImpacts.append(dictImpacts)
                listGlobalPefScore.append(pef_score)
                listGlobalTime.append(calc_finish)
                
            except Exception as e:
                print('  . calculation failed: %s' % e)

    listImpacts.append({'Global PEF Score': mean(listGlobalPefScore), 'Global time': sum(listGlobalTime)})
    print('All done; total runtime', ar.now() - start)
    
    return listImpacts

    
def prepare_setup(client: olca.Client, method_name: str) -> olca.CalculationSetup:
    """ Prepare the calculation setup with the LCIA method with the given name.
        Note that this is just an example. You can of course get a method by
        ID, calculate a system with all LCIA methods in the database etc.
    """
    method = client.find(olca.ImpactMethod, method_name)
    if method is None:
        sys.exit('Could not find LCIA method %s' % method_name)
    setup = olca.CalculationSetup()
    # currently, simple calculation, contribution analysis, and upstream
    # analysis are supported
    setup.calculation_type = olca.CalculationType.CONTRIBUTION_ANALYSIS
    setup.impact_method = method
    # amount is the amount of the functional unit (fu) of the system that
    # should be used in the calculation; unit, flow property, etc. of the fu
    # can be also defined; by default openLCA will take the settings of the
    # reference flow of the product system
    setup.amount = 1.0
    return setup


def set_parameters(setup: olca.CalculationSetup, parameters: pd.DataFrame,
                   parameter_set: int):
    """ Set the parameters of the given parameter set (which is the
        corresponding column in the data frame) to the calculation setup.
    """
    # for each parameter in the parameter set we add a parameter
    # redefinition in the calculation setup which will set the parameter
    # value for the respective parameter just for the calculation (without
    # needing to modify the database)
    setup.parameter_redefs = []
    for param in parameters.index:
        redef = olca.ParameterRedef()
        redef.name = param
        redef.value = parameters.loc[param, parameter_set]
        setup.parameter_redefs.append(redef)


def export_and_dispose(client: olca.Client, result: olca.SimpleResult, path: str):
    """ Export the given result to Excel and dispose it after the Export
        finished.
    """
    try:
        print('  . export result to', path)
        start = ar.now()
        client.excel_export(result, path)
        time = ar.now() - start
        print('  . export finished after', time)
        print('  . dispose result')
        client.dispose(result)
        print('  . done')
    except Exception as e:
        print('ERROR: Excel export or dispose of %s failed' % path)
