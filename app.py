from flask import Flask, render_template, request, jsonify
import pandas as pd
from script import main

app = Flask(__name__)
app.secret_key = "keepthissecret"
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True

@app.route('/', methods=['GET', 'POST'])
def index():
    """
    Récupérer les données du formulaire
    pour calculer les impacts environnementaux
    """
    if request.method == 'POST': 
        try:
            # Récupérer celles des données uploadées
            impactDetails = request.files['myfile']
            parameters = pd.read_csv(impactDetails, sep=";", header=None, index_col=0)
            return jsonify(main(parameters))

        except:
            # Sinon les données du formulaire
            impactDetails = request.form
            parameters = pd.DataFrame.from_dict(impactDetails, orient='index')
            return jsonify(main(parameters))
        
        # finally:
    
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)